# [1.1.0](https://gitlab.com/HerrShmulke/teste/compare/v1.0.2...v1.1.0) (2022-10-26)


### Bug Fixes

* asdsds ([ff115b3](https://gitlab.com/HerrShmulke/teste/commit/ff115b34fa323d1edb33bc0dd76892d2ea3bd8df))
* kfass ([270848f](https://gitlab.com/HerrShmulke/teste/commit/270848fa105fcee01bcabc7a1e29696354d17cea))


### Features

* feature4 ([8c04acc](https://gitlab.com/HerrShmulke/teste/commit/8c04acc6c2c5d331c4f38b452759b2ffc99218bb))

## [1.0.2](https://gitlab.com/HerrShmulke/teste/compare/v1.0.1...v1.0.2) (2022-10-26)


### Bug Fixes

* **some-subject:** change some logic ([0af9030](https://gitlab.com/HerrShmulke/teste/commit/0af903024d2cd84dd231172d2ff6612c1a4e282b))
* word to world ([0740634](https://gitlab.com/HerrShmulke/teste/commit/07406349a9fc246a308bef175f0ce831c81adacb))

## [1.0.1](https://gitlab.com/HerrShmulke/teste/compare/v1.0.0...v1.0.1) (2022-10-26)


### Bug Fixes

* releaserc ([d8db687](https://gitlab.com/HerrShmulke/teste/commit/d8db687f29daa03a50975b3cd8dff2e5080be332))

# 1.0.0 (2022-10-26)


### Bug Fixes

* repository url ([61155a5](https://gitlab.com/HerrShmulke/teste/commit/61155a555b7a960fb754d9eef491daa8bebdc903))
